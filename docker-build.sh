#!/bin/sh

docker build "$@"

dockerfile="Dockerfile"
while test $# -gt 0; do
    case "$1" in
        -f|--file)
            shift
            dockerfile=$1
            shift
            ;;
        -f=*)
            dockerfile=${1#"-f="}
            shift
            ;;
        --file=*)
            dockerfile=${1#"--file="}
            shift
            ;;
        *)
	    shift
            ;;
    esac
done
lockfile="$dockerfile-lock"

echo "Make $lockfile from $dockerfile"

cp $dockerfile $lockfile
grep ^FROM $lockfile | while read -r line ; do

    image=`echo $line | cut -d" " -f2`
    digest=`docker inspect --format='{{index .RepoDigests 0}}' $image`

    echo "$image > $digest"

    sed -i -e "s/$image/$digest/g" $lockfile
done
