FROM nginx:alpine

MAINTAINER Valentin LORTET <valentin@lortet.io>

RUN rm /etc/nginx/conf.d/*
COPY nginx/ /etc/nginx/

WORKDIR /usr/share/nginx/html/
RUN rm *
COPY www/ ./
